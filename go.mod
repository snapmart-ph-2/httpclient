module bitbucket.org/snapmartinc/httpclient

go 1.12

require (
	bitbucket.org/snapmartinc/logger v0.0.0-20190722102907-70e1fed01587
	bitbucket.org/snapmartinc/trace v0.0.0-20190925102910-0918afc8a51f
	bitbucket.org/snapmartinc/user-service-client v0.0.0-20190916112339-fefd7d2a4d59 // indirect
	github.com/newrelic/go-agent v2.14.1+incompatible
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/opentracing/opentracing-go v1.1.0
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/stretchr/testify v1.4.0
	gopkg.in/redis.v5 v5.2.9 // indirect
)
